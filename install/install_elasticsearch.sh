#!/bin/bash
# @Date   : 2020-11-25 10:40:41
# @Author : Shen Chucheng
# @MAIL   : chuchengshen@fuzhi.ai
# @Desc   : Elasticsearch 一键安装启动脚本
# @source : https://www.elastic.co/guide/en/elasticsearch/reference/7.6/docker.html

# 默认参数
# version 版本
version=7.6.1

# 映射端口
port=9200
port2=9300

# docker 运行容器名
name=es

# 配置文件目录(挂载目录)
# 宿主机
configdir=/opt/elk/elasticsearch/config
# docker
dconfigdir=/usr/share/elasticsearch/config

# 数据
datadir=/opt/elk/elasticsearch/data
ddatadir=/usr/share/elasticsearch/data

# 日志
logdir=/opt/elk/elasticsearch/logs
dlogdir=/usr/share/elasticsearch/logs


# 配置文件名
filename=elasticsearch.yml


default_version=$version
default_port=$port
default_port2=$port2
default_configdir=$configdir
default_logdir=$logdir
default_datadir=$datadir
default_filename=$filename
default_name=$name
usage() {
    echo "Usage:"
    echo "  $0 [-p port] [-P port2] [-c configDir] [-l logDir] [-d dataDir] [-v version] [-n name]  "
    echo "Description:"
    echo "    port：     宿主机映射docker端口9200 默认 $default_port"
    echo "    port2：    宿主机映射docker端口9300 默认 $default_port2"
    echo "    configDir：配置文件挂载目录 默认 $default_configdir"
    echo "    logDir：   日志文件挂载目录 默认 $default_logdir"
    echo "    dataDir：  数据文件挂载目录 默认 $default_datadir"
    echo "    version：  docker 镜像标签 默认 $default_version"
    echo "    name：     docker 启动容器名 默认 $default_name"
    exit -1
}


while getopts hp:c:l:d:P:n: option
do
   case "${option}"  in  
                p) port=${OPTARG};;
                P) port2=${OPTARG};;
                c) configdir=${OPTARG};;
                d) datadir=${OPTARG};;
                l) logdir=${OPTARG};;
                n) name=${OPTARG};;
                v) version=${OPTARG};;
                h) usage;;
                ?) usage;;
   esac
done

# 权限检测
if ! [ "$EUID" = "0" ]; then 
  echo '安装脚本需要 root 权限，请使用 root 用户，或者 +sudo 执行'
  exit 1
fi

# 环境检测
if ! [ -x "$(command -v docker)" ]; then
  echo '未检测到docker程序，请先安装docker' >&2
  exit 1
fi

if ! [ -x "$(command -v systemctl)" ]; then
  echo '未检测到systemctl工具，请先安装systemd' >&2
  exit 1
fi

if ! [ "$(systemctl is-active docker)" = 'active' ]; then
  echo 'docker 未启动' 
  echo '开始启动docker'
  systemctl start docker.service
  if ! [ "$(systemctl is-active docker)" = 'active' ]; then
    echo 'docker 启动失败' >&2
    exit 1
  fi
  echo '启动docker成功'
fi

# 参数确认
echo "参数："
echo "port:      $port"
echo "port2:     $port2"
echo "configDir: $configdir"
echo "dataDir:   $datadir"
echo "logDir:    $logdir"
echo "version:   $version"
echo "name:      $name"

# 配置文件路径
configfile=$configdir/$filename
dconfigfile=$dconfigdir/$filename
images=docker.elastic.co/elasticsearch/elasticsearch

# docker 安装
if ! [ "$(docker images $images:$version -q)" ]; then
  echo "开始安装 $images:$version"
  docker pull $images:$version
  if ! [ "$(docker images $images:$version -q)" ]; then
    echo "拉取 docker 镜像 $images:$version 失败" >&2
    echo '请检查网络或者镜像标签是否异常，然后执行命令以下：'
    echo "  docker pull $images:$version  "
    echo '镜像拉取成功后，再重新执行安装脚本'
    exit 1
  fi
fi


for i in $datadir $logdir $configdir
do
  if [ ! -d "$i" ]; then
    echo "挂载文件目录不存在，生成目录 $i "
    mkdir -p $i
    if [ ! -d "$i" ]; then
      echo '生成目录失败' >&2
      echo "请检查挂载目录  $i "
      exit 1
    fi
  fi
done

if [ ! -f "$configfile" ]; then
  echo "创建配置文件 $configfile"
  touch $configfile 
fi

if [ ! -s "$configfile" ]; then
  echo '正在生成配置文件模板'
  tee $configfile << \EOF
# ======================== Elasticsearch Configuration =========================
#
# NOTE: Elasticsearch comes with reasonable defaults for most settings.
#       Before you set out to tweak and tune the configuration, make sure you
#       understand what are you trying to accomplish and the consequences.
#
# The primary way of configuring a node is via this file. This template lists
# the most important settings you may want to configure for a production cluster.
#
# Please consult the documentation for further information on configuration options:
# https://www.elastic.co/guide/en/elasticsearch/reference/index.html
#
# ---------------------------------- Cluster -----------------------------------
#
# Use a descriptive name for your cluster:

cluster.name: es-test-cluster
#
# ------------------------------------ Node ------------------------------------
#
# Use a descriptive name for the node:

node.name: es-test-master

node.master: true

node.data: false

#node.ingest: false

#node.ml: false
#xpack.ml.enabled: true

#cluster.remote.connect: false
#
# Add custom attributes to the node:
#
#node.attr.rack: r1
#
# ----------------------------------- Paths ------------------------------------
#
# Path to directory where to store the data (separate multiple locations by comma):
#
path.data: /usr/share/elasticsearch/data
#
# Path to log files:
#
path.logs: /usr/share/elasticsearch/logs
#
# ----------------------------------- Memory -----------------------------------
#
# Lock the memory on startup:
#
#bootstrap.memory_lock: true
#
# Make sure that the heap size is set to about half the memory available
# on the system and that the owner of the process is allowed to use this
# limit.
#
# Elasticsearch performs poorly when the system is swapping the memory.
#
# ---------------------------------- Network -----------------------------------
#
# Set the bind address to a specific IP (IPv4 or IPv6):

network.host: 0.0.0.0
network.publish_host: 192.168.50.197
#
# Set a custom port for HTTP:

http.port: 9200

transport.tcp.port: 9300
#
# For more information, consult the network module documentation.
#
# --------------------------------- Discovery ----------------------------------
#
# Pass an initial list of hosts to perform discovery when this node is started:
# The default list of hosts is ["127.0.0.1", "[::1]"]
#
discovery.seed_hosts:
  - 192.168.50.197:19200
  - 192.168.50.197:29200
#
# Bootstrap the cluster using an initial set of master-eligible nodes:

cluster.initial_master_nodes:
  - test-es-master
  - test-es-node2
  - test-es-node3
#
# For more information, consult the discovery and cluster formation module documentation.
#
# ---------------------------------- Gateway -----------------------------------
#
# Block initial recovery after a full cluster restart until N nodes are started:
#
gateway.recover_after_nodes: 1
#
# For more information, consult the gateway module documentation.
#
# ---------------------------------- Various -----------------------------------
#
# Require explicit names when deleting indices:
#
#action.destructive_requires_name: true


http.cors.enabled: true
http.cors.allow-origin: "*"
EOF
  echo '初始化配置文件模板完成，可通过命令查看配置文件内容'
  echo " cat $configfile "
fi

chown -R 1000:1000 $datadir
chown -R 1000:1000 $logdir
chown -R 1000:1000 $configdir

cmd="docker run --restart=always -d -p $port:9200 -p $port2:9300 --name $name \
    -v $configfile:$dconfigfile -v $logdir:$dlogdir -v $datadir:$ddatadir $images:$version "

if ! [ "$(docker ps -a -q -f name=$name)" ]; then
  $cmd
  if ! [ "$(docker ps -q -f name=$name)" ]; then
    echo "$images:$version  docker 启动失败，请检查启动命令" >&2
    echo " $cmd " >&2
    exit 1
  fi

else
  echo "容器'$name'已经存在，请重新指定'-n'参数，并确保容器名未重复；或者检查$name($images:$version)是否已经在运行" >&2
  echo "若确定指定启动容器名为'$name'，请用以下命令检查容器并确认删除，然后重新执行此脚本" >&2
  echo " 检查： docker ps -a -f name=$name "
  echo " 删除： docker  kill $name && docker rm $name "
  exit 1
fi

if ! [ $(systemctl is-enabled docker) = 'enabled' ]; then 
  echo '设置 docker 开机启动' 
  systemctl enable docker.service
  if ! [ $(systemctl is-enabled docker) = 'enabled' ]; then 
    echo '设置 docker 开机启动失败，请尝试执行以下命令' >&2
    echo ' systemctl enable docker.service ' >&2
    exit 1
  fi
fi
echo " $images:$version 安装成功！$name 启动成功! " 
echo " 查看配置文件路径：$configfile "
echo " 查看 server 信息访问 127.0.0.1:$port "
