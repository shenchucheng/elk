#!/bin/bash
# @Date   : 2020-11-25 11:39:05
# @Author : Shen Chucheng
# @MAIL   : chuchengshen@fuzhi.ai
# @Desc   : Filebeat 一键安装启动脚本
 
# 默认参数
# version  版本
version=7.6.1

# 映射端口
port=5601

# docker 运行容器名
name=filebeat 

# 镜像
images=docker.elastic.co/beats/filebeat


# 配置文件目录(挂载目录)
# 宿主机
configdir=/opt/elk/filebeat/config
# docker
dconfigdir=/usr/share/filebeat

# 数据
datadir=/opt/elk/filebeat/data
ddatadir=/usr/share/filebeat/data

# 日志
logdir=/opt/elk/filebeat/logs
dlogdir=/usr/share/filebeat/logs


# 配置文件名
filename=filebeat.yml


default_version=$version
default_port=$port
default_configdir=$configdir
default_logdir=$logdir
default_datadir=$datadir
default_filename=$filename
default_name=$name

usage() {
    echo "Usage:"
    echo "  $0 [-p port] [-P port2] [-c configDir] [-l logDir] [-d dataDir] [-v version] [-n name]  "
    echo "Description:"
    echo "    port：     宿主机映射docker端口 默认 $default_port"
    echo "    configDir：配置文件挂载目录 默认 $default_configdir"
    echo "    logDir：   日志文件挂载目录 默认 $default_logdir"
    echo "    dataDir：  数据文件挂载目录 默认 $default_datadir"
    echo "    version：  docker 镜像标签 默认 $default_version"
    echo "    name：     docker启动容器名 默认 $default_name"
    exit -1
}


while getopts hp:c:l:d:n: option
do
   case "${option}"  in  
                p) port=${OPTARG};;
                c) configdir=${OPTARG};;
                d) datadir=${OPTARG};;
                l) logdir=${OPTARG};;
                n) name=${OPTARG};;
                v) version=${OPTARG};;
                h) usage;;
                ?) usage;;
   esac
done

# 权限检测
if ! [ "$EUID" = "0" ]; then 
  echo '安装脚本需要 root 权限，请使用 root 用户，或者 +sudo 执行'
  exit 1
fi

# 环境检测
if ! [ -x "$(command -v docker)" ]; then
  echo '未检测到docker程序，请先安装docker' >&2
  exit 1
fi

if ! [ -x "$(command -v systemctl)" ]; then
  echo '未检测到systemctl工具，请先安装systemd' >&2
  exit 1
fi

if ! [ "$(systemctl is-active docker)" = 'active' ]; then
  echo 'docker 未启动' 
  echo '开始启动docker'
  systemctl start docker.service
  if ! [ "$(systemctl is-active docker)" = 'active' ]; then
    echo 'docker 启动失败' >&2
    exit 1
  fi
  echo '启动docker成功'
fi

# 参数确认
echo "参数："
echo "port:      $port"
echo "configDir: $configdir"
echo "dataDir:   $datadir"
echo "logDir:    $logdir"
echo "version:   $version"
echo "name:      $name"

# 配置文件路径
configfile=$configdir/$filename
dconfigfile=$dconfigdir/$filename

# docker 安装
if ! [ "$(docker images $images:$version -q)" ]; then
  echo "开始安装 $images:$version"
  docker pull $images:$version
  if ! [ "$(docker images $images:$version -q)" ]; then
    echo "拉取 docker 镜像 $images:$version 失败" >&2
    echo '请检查网络或者镜像标签是否异常，然后执行命令以下：'
    echo "  docker pull $images:$version  "
    echo '镜像拉取成功后，再重新执行安装脚本'
    exit 1
  fi
fi


for i in $datadir $logdir $configdir
do
  if [ ! -d "$i" ]; then
    echo "挂载文件目录不存在，生成目录 $i "
    mkdir -p $i
    if [ ! -d "$i" ]; then
      echo '生成目录失败' >&2
      echo "请检查挂载目录  $i "
      exit 1
    fi
  fi
done

if [ ! -f "$configfile" ]; then
  echo "创建配置文件 $configfile"
  touch $configfile 
fi

if [ ! -s "$configfile" ]; then
  echo '正在生成配置文件模板'
  tee $configfile << \EOF
filebeat.config:
  modules:
    path: ${path.config}/modules.d/*.yml
    reload.enabled: true
    reload.period: 30s

name: filebeat_test
tags: ["test"]

processors:
  - add_host_metadata: ~
  - add_cloud_metadata: ~
  - add_docker_metadata: ~
  - add_kubernetes_metadata: ~

output.logstash:
  hosts: ["192.168.50.197:4560"]

# output.elasticsearch:
#   hosts: ["192.168.50.197:9200"]

logging.level: debug
logging.to_files: true
logging.path: /usr/share/filebeat/logs/filebeat
logging.name: filebeat

EOF
  echo '初始化配置文件模板完成，可通过命令查看配置文件内容'
  echo " cat $configfile "
fi

chown -R 1000:1000 $datadir
chown -R 1000:1000 $logdir
chown -R 1000:1000 $configdir

cmd="docker run --restart=always -d --name $name \
    -v $configfile:$dconfigfile -v $logdir:$dlogdir -v $datadir:$ddatadir $images:$version "


if ! [ "$(docker ps -a -q -f name=$name)" ]; then
  $cmd
  if ! [ "$(docker ps -q -f name=$name)" ]; then
    echo "$images:$version  docker 启动失败，请检查启动命令" >&2
    echo " $cmd " >&2
    exit 1
  fi
else
  echo "容器'$name'已经存在，请重新指定'-n'参数，并确保容器名未重复；或者检查$name($images:$version)是否已经在运行" >&2
  echo "若确定指定启动容器名为'$name'，请用以下命令检查容器并确认删除，然后重新执行此脚本" >&2
  echo " 检查： docker ps -a -f name=$name "
  echo " 删除： docker  kill $name && docker rm $name "
  exit 1
fi

if ! [ $(systemctl is-enabled docker) = 'enabled' ]; then 
  echo '设置 docker 开机启动' 
  systemctl enable docker.service
  if ! [ $(systemctl is-enabled docker) = 'enabled' ]; then 
    echo '设置 docker 开机启动失败，请尝试执行以下命令' >&2
    echo ' systemctl enable docker.service ' >&2
    exit 1
  fi
fi
echo " $images:$version 安装成功！$name 启动成功! " 
echo " 查看配置文件路径：$configfile "
