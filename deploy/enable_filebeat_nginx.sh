#!/bin/bash
# @Date   : 2020-11-25 21:16:21
# @Author : Shen Chucheng
# @MAIL   : chuchengshen@fuzhi.ai
# @Desc   : 
 
if ! [ -x "$(command -v filebeat)" ]; then
  echo '请先安装filebeat' >&2
  exit 1
fi

if ! [ "$EUID" = "0" ]; then 
  echo '安装脚本需要 root 权限，请使用 root 用户，或者 +sudo 执行'
  exit 1
fi

if [ $(filebeat modules enable nginx) ]; then
fi
