#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2020-10-27 14:36:37
# @Author  : Shenchucheng (chuchengshen@fuzhi.ai)
# @Desc    : Elasticsearch Client for Python
# 实现简单测试、多线程版本、多进程版本、协程版本
# 注意logger的调用，logger上传成功的信息不应进入ES，可能无限循环


import os
from time import sleep
from queue import Empty
from threading import Lock
from collections import OrderedDict
from elasticsearch import Elasticsearch, logger
from elasticsearch.helpers import bulk
from elasticsearch.exceptions import TransportError

from utils.helper import Statistics


class EsClient(Elasticsearch):
    RUNNING_MODE_MAP = OrderedDict([
        ('simple', 0),
        ('multithreading', 1),
        ('multiproccessing', 2)
    ])

    def __init__(self, hosts=None, **kwagrs):
        if not hosts:
            hosts = "localhost:9200"
        self._max_tries = kwagrs.pop('es_max_tries', 0)
        self._interval = kwagrs.pop('es_interval', 10)
        self.__status = None  # 0: stop 1: running 2: stopping
        self.push = self._push
        self._start_push = None
        self.__start_process = None 

        super(EsClient, self).__init__(hosts, **kwagrs)

    def test_connect(self, params=None, headers=None):
        """
        Test whether the cluster is running.
        see self.ping()
        """
        hosts = self.transport.hosts
        try:
            r = self.transport.perform_request(
                "HEAD", "/", params=params, headers=headers
            )
            logger.info('Connected to the Elasticsearch Server '\
                 '[{}] Successfully'.format(hosts))
            return r 
        except TransportError as e:
            logger.error('Fail to Connect to the Elasticsearch '\
                'Server [{}]'.format(hosts), exc_info=True)
            raise e 
    
    def start(self, mode='simple'):
        """
        :desc start the ES. client in the specified mode 
        :param mode: the client running mode, defaults to 'simple'
        """
        if isinstance(mode, int):
            des = list(self.RUNNING_MODE_MAP.keys())[mode]
        else:
            des = mode
            mode = self.RUNNING_MODE_MAP[mode]
        if self.__status == 1: # running
            self.stop()
        if mode == 0:
            self.push = self._simple_push
            if not hasattr(self, '__statistics'):
                self.__statistics = Statistics()
            if not hasattr(self, 'push_error'):
                self.push_error = []
        elif mode == 1:
            from queue import Queue
            from threading import Thread
            self.push = self._multithreading_push
            self._msgs = Queue(-1)
            self._thread = Thread(target=self.process)
            self._thread.setDaemon(True)
            self._thread.start()
        elif mode == 2: 
            from multiprocessing import Queue  # 该模式下会再起一个线程  #TODO 网络通讯  redis
            from multiprocessing import Process
            self.push = self._multiprocessing_push
            self._msgs = Queue(-1)
            self._process = Process(target=self.process)
            self._process.start()
        else:
            raise ValueError()
        self.__status = 1
        self.mode = mode 
        logger.info('ElasticSearch Client is Running in {} mode'.format(des))

    def process(self):
        """
        :desc for multithreading and multiproccessing mode
        """
        def msgs():
            while 1:
                try:
                    yield self._msgs.get_nowait()
                except Empty:
                    break
        while 1:
            with Lock():
                try:
                    self._start_push= True
                    # self.bulk
                    bulk(self, msgs())
                except KeyboardInterrupt:
                    bulk(self, msgs())
                    break
                finally:
                    self._start_push = False
                    if self.__status == 2:
                        self.__status = 0
                        break
            sleep(self._interval)

    def stop(self):
        """
        :desc stop the pushing thread 
        """
        if self.mode in [1, 2]:
            self.__status = 2
            while self._thread.isAlive():
                sleep(1)
        self.__status = None
        logger.info('Now the Es Client has Stopped') 

    def _push(self, msg):
        """
        :desc push the json object to the server 
              actual method depend on the mode when client start assigned
        :param msg: json dict msg
        """
        raise NotImplementedError('Before Calling Push Method '\
            'Please Start this Client Firstly')    
    
    def _simple_push(self, msg):
        """
        :desc simple mode for test, no recommended for product
        :param msg: json dict msg
        """
        try:
            self._start_push = True
            # print(msg)
            index = msg.pop('_index')
            body = msg.pop('_source')
            _id = msg.get('_id')
            doc_type = msg.get('_type')
            r = self.index(index=index, body=body, id=_id, doc_type=doc_type)
            self.__statistics[r.get('result')] += 1
            logger.debug(self.__statistics)
        except Exception as e:
            self.push_error.append(msg)
            logger.warn('push error for {}'.format(e))
        finally:
            self._start_push = False


    def _multithreading_push(self, msg):
        """
        :desc push the msg by multiple threading  
        :param msg: json dict msg
        """
        try:
            self._msgs.put(msg)
        except Exception:
            raise

    def _multiprocessing_push(self, msg):
        """
        :desc push the msg by multiple processing
        :param msg: json dict msg
        """
        try:
            self._msgs.put(msg)
        except Exception:
            raise

def main():
    es = EsClient()
    es.test_connect()
    es = EsClient('localhost:9201')
    es.test_connect()


if __name__ == "__main__":
    main()
     
