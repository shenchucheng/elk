#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2020-11-03 00:54:31
# @Author  : Shen Chucheng (chuchengshen@fuzhi.ai)
# @Desc    : 


import asyncio

from logging import getLogger
from collections import deque
from elasticsearch import AsyncElasticsearch, logger
from elasticsearch.helpers import async_bulk


class AsyncEsClient(AsyncElasticsearch):
    def __init__(self, hosts=None, **kwargs):
        if not hosts:
            hosts = 'localhost:9200'
        interval = kwargs.pop('interval', 10)
        super(AsyncEsClient, self).__init__(hosts=hosts, **kwargs)
        self.msgs = deque()
        self.interval = interval
        self.__status = None
        self._start_push = None 

    def push(self, msg):
        self.msgs.append(msg)

    async def start(self):
        def msgs():
            while 1:
                try:
                    yield self.msgs.pop()
                except IndexError:
                    break
        self.__status = 1
        while 1:
            sleep = asyncio.sleep(self.interval)
            try:
                self._start_push = True
                r = await async_bulk(self, msgs())
                print(r)
            except KeyboardInterrupt:
                await async_bulk(self, msgs())
                self.__status = 2
            except Exception:
                pass
            finally:
                self._start_push = False
                if self.__status == 2:
                    self.__status = 0
                    break
            await sleep
        
    def stop(self):
        self.__status = 2
        logger.info('Set the ES. client status to 2(stopping)')
    
    async def wait_stop(self):
        if self.__status == 0:
            logger.info('Es. client has stopped Already')
            return True
        if self.__status == 2:
            while self.__status:
                await asyncio.sleep(1)
                if self.__status == 1:
                    return False
            logger.info('Es. client is stop now')
            return True 
        else:
            return False

def main():
    from utils import eslogger
    pass

if __name__ == "__main__":
    main()
     
