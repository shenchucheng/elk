#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2020-10-28 09:51:32
# @Author  : Shen Chucheng (chuchengshen@fuzhi.ai)
# @Desc    : Elasearch Json Formatter and Logging Handle 


import os 
import time
import logging
import uuid
import socket
import json
from datetime import datetime
from logging import Formatter, Handler
from collections import OrderedDict

import utils.esclient
from utils.network import get_mechine_id
from utils.logging import Singleton, get_logger
from utils.logging import ColoredFormatter, MultiProRotatingFileHandler
from utils.esclient import EsClient

logger = logging.getLogger('Eslogger')


class EsJsonFormatter(Formatter):
    """
    :desc Format log data to json for elasticsearch
    """
    def __init__(self, json_const=None, dumps=None, *args, **kwargs):
        """
        :param json_const: Added to the log_record, defaults to None
        :param dumps: Serialize obj to a JSON formatted str, defaults to json.dumps
        :param line_info: add line info to the log_record
        """
        self.json_const = json_const
        self.dumps = dumps or json.dumps
        self.line_info = kwargs.pop("line_info", False)
        self.json_format = kwargs.pop("json_format", True)
        super(EsJsonFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        """
        :desc jsonify the record for ES.
        """
        try:
            log_record = OrderedDict()
        except NameError:
            log_record = {}
        # TODO: check the time zone 
        log_record['timestamp'] = datetime.fromtimestamp(record.created).isoformat()  
        log_record['level'] = record.levelname
        log_record['name'] = record.name
        if self.json_const:
            log_record.update(self.json_const)
        if isinstance(record.msg, dict):
            extra = record.msg 
            record.msg = extra.pop('msg', '')
        else:
            extra = dict()
        s = record.getMessage()
        if record.exc_info:
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":
                s = s + "\n"*2
            s = s + record.exc_text
        try:
            if record.stack_info:
                if s[-1:] != "\n":
                    s = s + "\n"*2
                s = s + self.formatStack(record.stack_info)
        except AttributeError:
            pass

        record.message = s
        log_record['message'] = record.message 
        if self.line_info:
            extra['location'] = "{}:{}".format(record.pathname, record.lineno)
        if extra:
            log_record['extra'] =  extra
        return self.process_record(record, log_record)

    def process_record(self, record, log_record):
        """
        :desc Override this function for customization 
        """
        if self.json_format: 
            return self.dumps(log_record)
        else:
            return log_record


class EsHandler(Handler):
    """
    :desc logging handle for ES.
    """
    def __init__(self, es_index, es_index_rotate=True, es_clinet=None,
            formatter=None, *args, **kwargs):
        """
        :param es_index: the ES. index name for the log record
        :param es_index_rotate: rotate the log, defaults to True
        :param es_clinet: Specify the ES. client
        :param formatter: defaults to EsJsonFormatter
        """
        if es_index_rotate:
            time_fmt = kwargs.pop('es_index_time_fmt', '%Y_%m_%d')
            if '{' not in es_index:
                es_index = '_'.join((es_index, '{timefmt}'))
        es_index_type = kwargs.pop('es_index_type', None) or 'log'
        super(EsHandler, self).__init__(*args, **kwargs)
        self.es_index = es_index
        self.es_index_type = es_index_type
        self.es_index_rotate = es_index_rotate
        if self.es_index_rotate:
            self.es_index_time_fmt = time_fmt
            
        if not es_clinet:
            es_clinet = EsClient()
            try:
                es_clinet.start(kwargs.pop('es_client_mode', 0))
                es_clinet.test_connect()
                logger.debug('Create ES. Client {} automatically'.format(str(es_clinet)))
            except Exception:
                logger.error('Fail to connect the ES. Sever automatically'\
                    'Please pass the "es_clinet" parameter')
                raise

        self.es_clinet = es_clinet
        self.formatter = formatter or EsJsonFormatter(json_format=False)

    def index_time_format(self, timestamp=None):
        return time.strftime(self.es_index_time_fmt, time.localtime(timestamp or time.time())) 

    def _make_rotate_index(self, timefmt=None, timestamp=None):
        if timefmt:
            index = self.es_index.format(timefmt=timefmt)
        else:
            index = self.es_index.format(self.index_time_format(timestamp))
        if index != self.__dict__.get('_index_name', None):
            # self.es_index.indices()  # 创建index
            pass
        return index

    def _make_index(self, **kwargs):
        return self.es_index   
        
    @property
    def es_index_rotate(self):
        return self.__es_index_rotate
    
    @es_index_rotate.setter
    def es_index_rotate(self, value):
        if value:
            self.make_index = self._make_rotate_index
        else:
            self.make_index = self._make_index
        self.__es_index_rotate = bool(value)

    def emit(self, record):
        try:
            msg = {
                "_index": self.make_index(timefmt=record.created),
                "_doc_type": self.es_index_type,
                "_source": self.format(record)
                }
            self.es_clinet.push(msg)
        except Exception:
            self.handleError(record)


class EsFileHandler(EsHandler, MultiProRotatingFileHandler):
    """
    :desc Backup the ES. log locally
    """
    def __init__(self, es_index, filename='', *args, **kwargs):
        if not filename:
            filename = es_index + '.log'
        super(EsFileHandler, self).__init__(es_index=es_index, filename=filename,
             *args, **kwargs)        

    def emit(self, record):
        try:
            if self.es_clinet._start_push:
                return      
            timefmt = self.index_time_format(record.created)
            data = self.format(record)
            msg = {
                "_index": self.make_index(timefmt=timefmt),
                "_doc_type": self.es_index_type,
                "_source": data 
                }
            if not self.es_clinet.push(msg):
                msg = self.es_clinet.transport.serializer.dumps(data)
                if self._check_basefile():
                    self._recreate_basefile()
                stream = self.stream
                stream.write(msg + self.terminator)
                self.flush()   
        except Exception:
            self.handleError(record)
    
    def flush(self):
        MultiProRotatingFileHandler.flush(self)


@Singleton
class DmsLogger:
    def set_fixed_params(self, es_on=True, app_name="default_app", service_name="default_service",
        mechine_name=None, verbose=False, ondisk=False, file_path=None, index_name=None):
        self.__es_on = es_on
        self.__index_name = index_name
        self.__app_name = app_name
        self.__service_name = service_name
        self.__mechine_name = mechine_name or get_mechine_id()
        self.__verbose = verbose
        self.__ondisk = ondisk
        self.__file_path = file_path
        self.__json_const = None
        self.__check_params()

    def __check_params(self):
        if self.__es_on:
            if self.__index_name is None:
                raise ValueError("Please pass the parameter of index_name")
            if self.__file_path is None:
                self.__file_path = self.__index_name + '.log'
            self.__json_const = {
                "app_name": self.__app_name, 
                "service_name": self.__service_name, 
                "mechine_name": self.__mechine_name
            }
        if self.__ondisk:
            if self.__file_path is None:
                raise ValueError("file_path: {} should be a absolute path".format(self.__file_path))

    def get_fmt(self):
        """
        :desc log record format
        :return:
        """
        fmt = "%(asctime)s %(levelname)s %(filename)s:%(lineno)d " \
              "app: {}, service: {}, message: %(message)s".format(self.__app_name, self.__service_name)
        return fmt

    def get_logger(self, logger_name):
        """
        :desc get logger
        :param logger_name: logger name
        :return:
        """
        logger = _get_logger(logger_name=logger_name, verbose=self.__verbose, ondisk=self.__ondisk,
                            index_name=self.__index_name, file_path=self.__file_path, fmt=self.get_fmt())
        return logger


def _get_logger(logger_name, es_on=True, index_name=None, verbose=False, 
        ondisk=False, file_path=None, fmt=""):
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logger = logging.getLogger(name=logger_name)
    log_level = logging.DEBUG if verbose else logging.INFO
    logger.setLevel(log_level)
    if not logger.handlers and es_on:
        if ondisk:
            handler = EsFileHandler(es_index=index_name)
        else:
            handler = EsHandler(es_index=index_name)
        handler.es_clinet.start()
        handler.setLevel(level=log_level)
        logger.handlers = []
        logger.addHandler(handler)
        return logger 
    return get_logger(logger_name, verbose, ondisk, file_path, fmt)


def main():
    dms = DmsLogger()
    dms.set_fixed_params(es_on=True, index_name='eslogger_test', ondisk=True)
    logger = dms.get_logger('eslogger_test')
    utils.esclient.logger.setLevel(10)
    handler = logging.StreamHandler()
    handler.setLevel(10)
    utils.esclient.logger.addHandler(handler)    
    logger.debug('test debug')
    logger.info('test info')
    logger.warn('test warn')
    logger.error('test error')
 
if __name__ == "__main__":
    main()
     

