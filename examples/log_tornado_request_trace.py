#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
# @Date    : 2020-11-02 12:04:08
# @Author  : Shen Chucheng (chuchengshen@fuzhi.ai)
# @Desc    : 

import uuid
import contextvars
import tornado.web
import logging

from utils.eslogger import EsFileHandler, EsJsonFormatter, EsHandler
    

logger = logging.getLogger('tornadoRequestId_test')
request_id_var = contextvars.ContextVar("request_id")


class MyHandler(tornado.web.RequestHandler):
    # prepare is called at the beginning of request handling
    def prepare(self):
        # If the request headers do not include a request ID, let's generate one.
        request_id = self.request.headers.get("request-id") or str(uuid.uuid4())
        request_id_var.set(request_id)

    def get(self):
        info1 = trace_1()
        info2 = trace_2()
        info3 = trace_3()
        self.write("Info: {}, {}, {}".format(info1, info2, info3))


def trace_1():
    info = 'trace_1'
    logger = logging.getLogger(info)
    logger.info("this is trace in {}".format(info))
    return info 


def trace_2():
    info = 'trace_2'
    logger = logging.getLogger(info)
    logger.info("this is trace in {}".format(info))
    return info 


def trace_3():
    info = 'trace_3'
    logger = logging.getLogger(info)
    logger.info("this is trace in {}".format(info))
    return info 


class Filter(logging.Filter):
    def filter(self, record):
        try:
            record.request_id = request_id_var.get()
        except:
            record.request_id = None
        return True


class Formatter(EsJsonFormatter):
    def process_record(self, record, log_record):
        if getattr(record, 'request_id', None):
            if log_record.get('extra'):
                log_record['extra']['requestid'] = record.request_id
            else:
                log_record['extra'] = {'requestid': record.request_id}
        # print(log_record)
        return super(Formatter, self).process_record(record, log_record)


def main():
    handler = EsFileHandler('tornado_request_id_test', filename='/var/log/tornado/tornado_request_id_test.log')
    handler.addFilter(Filter())
    handler.formatter = Formatter()
    handler.formatter.line_info = True 
    handler.es_clinet.start(1)
    logging.basicConfig(level=20, handlers=[handler])
    port = 8008
    app = tornado.web.Application([(r"/", MyHandler),])    
    app.listen(port)
    print("Listening at http://localhost:%d/", port)
    tornado.ioloop.IOLoop.current().start()


# 支持异步
# def main():
#     from utils.asyncesclient import AsyncEsClient
#     es = AsyncEsClient()
#     handler = EsFileHandler('tornado_request_id_test', es_clinet=es,
#         filename='/var/log/tornado/tornado_request_id_test.log')
#     handler.addFilter(Filter())
#     handler.formatter = Formatter()
#     handler.formatter.line_info = True 
#     logging.basicConfig(level=20, handlers=[handler])
#     port = 8008
#     app = tornado.web.Application([(r"/", MyHandler),])    
#     app.listen(port)
#     print("Listening at http://localhost:%d/", port)
#     tornado.ioloop.IOLoop.current().spawn_callback(es.start)
#     tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
     
