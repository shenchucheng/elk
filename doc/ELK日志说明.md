# 1. **介绍**
- ELK
    >`ELK` 是**elastic**公司提供的一套完整的日志收集以及展示的解决方案，是三个产品的首字母缩写，分别是`ElasticSearch`、`Logstash` 和 `Kibana`。主要可用于问题排查/监控和预警/关联事件/数据分析等。

- dms ES.日志组件
    > 在`ELK` 的基础上，结合 `python` 的 `logging` 模块，提供 `ELK` 使用模板及相关工具，使`ELK`的使用更加方便。

# 2. **安装**
- **安装ELK**
  - [**ElasticSearch**](https://www.elastic.co/cn/downloads/elasticsearch)

  - [**Logstash**](https://www.elastic.co/cn/downloads/logstash)
  
  - [**Kibana**](https://www.elastic.co/cn/downloads/kibana)
  
  - [**Filebeat**](https://www.elastic.co/cn/downloads/beats/filebeat)

- **安装dms**
    ```
    git clone git@gitlab.com:DeepWisdomAI/infrasturcture/dms.git
    cd dms
    git checkout ES_log
    python3 -m pip install -r requirements.txt
    ```


# 3. **日志抽取方法**

- **内嵌于python程序**

    > 直接在 `python` 程序中通过 `ES.(ElasticSearch)` 客户端将所需日志上传至 `ES.` 服务端，这种方式不需要再通过 `FileBeat` 或者 `Logstash` 抽取日志。以下提供两个使用例子和一个应用实例。

  - 使用单独logger
    > 如果只需记录某个 `logger` ，直接给对应 `logger` 绑定 `EsHandle`
    ```python
    def main():
        import logging
        from utils.eslogger import EsHandler
        handler = EsHandler('es_index_test', es_index_rotate=False)
        logger = logging.getLogger('es_test')
        logger.addHandler(handler)
        logger.setLevel(logging.INFO)
        logger.debug('test debug')
        logger.info({'msg': 'test info', 'status': 'running'})
        logger.warning('test warn')
        logger.error('test error')
        # 以下注释可查询插入情况
        # print(handler.es_clinet.search(index=es_index_test))
    if __name__=='__main__':
        main()
    ```

  - 全局配置
    > 可通过 `logging.basicConfig` 进行全局配置；在已有项目基础上，只需要在程序主入口配置 `logging` 环境，无需改动其他代码，即可实现日志上传。一般情况下，日志上传 `ES.` 的同时，需要本地同时记录。使用 `EsFileHandler`，可实现本地日志上传的同时，上传日志至 `ES.`。
    ```
    def main():
        from logging import basicConfig
        from utils.eslogger import EsFileHandler  # 本地日志备份+ES.日志上传
        handler = EsFileHandler('es_test')
        handler.es_clinet.start(0)
        basicConfig(level=10, handlers=[handler])
        logger.info({'msg': 'test info', 'status': 'running'})
        logger.warning('test warn')
    ```

  - **tornado 应用例子**
    > 这个例子将本 `ES` 日志模块应用在 `tornado` 中，通过 `request_id` 追踪每次请求对应调用的所有日志。（**TODO**: 分布式调用日志追踪
    > 文件: `examples` 文件夹下的 `log_tornado_request_trace.py`
    > 运行并访问 `tornado web server` ，最终可以得到类似输出：
    ```json
    {..."name": "trace_1"..."extra": {"requestid": "4c927764-0e68-4ff8-85d6-e2f3bfffae97"}}
    {..."name": "trace_2"..."extra": {"requestid": "4c927764-0e68-4ff8-85d6-e2f3bfffae97"}}
    {..."name": "trace_3"..."extra": {"requestid": "4c927764-0e68-4ff8-85d6-e2f3bfffae97"}}
    {..."name": "tornado.access", "message": "200 GET / (::1) 1.02ms", "extra": {"requestid": "4c927764-0e68-4ff8-85d6-e2f3bfffae97"}}
    ```  
    > 即可由 `request_id` 追踪对应的请求，并进行分析。


  - 支持异步
    > 只需要将异步的 `EsClient` 传到 `EsHandle` 中，并将 `Eclient.start` 函数注册到异步的task中，就可以实现异步支持。同样以 `tornado` 为例，修改 `log_tornado_request_trace.py` 的 `main` 函数。
    ```    
    def main():
        from utils.asyncesclient import AsyncEsClient
        es = AsyncEsClient()
        handler = EsFileHandler('tornado_request_id_test', es_clinet=es,
            filename='/var/log/tornado/tornado_request_id_test.log')
        handler.addFilter(Filter())
        handler.formatter = Formatter()
        handler.formatter.line_info = True 
        logging.basicConfig(level=20, handlers=[handler])
        port = 8008
        app = tornado.web.Application([(r"/", MyHandler),])    
        app.listen(port)
        print("Listening at http://localhost:%d/", port)
        tornado.ioloop.IOLoop.current().spawn_callback(es.start)
        tornado.ioloop.IOLoop.current().start()
    ```

- **FileBeat** 或者 **Logstash** 采集日志
  > 相较于`Logstash`，`Filebeat` 所耗费资源更小，因此建议使用 `Filebeat` 进行日志采集。可以采用 `EsJsonFormatter` 将日志内容预格式化为json格式，再使用 `FileBeat` 采集，那将非常方便。

  - **FileBeat**
 
    > 根据官网教程安装后，先进行文件配置，以[filebeat 7.9](https://www.elastic.co/guide/en/beats/filebeat/6.2/configuration-filebeat-options.html) 进行 [**json格式解析**](https://www.elastic.co/guide/en/beats/filebeat/current/decode-json-fields.html) 的日志为例：
    > 修改 `filebeat` 配置文件（/etc/filebeat/） 以下字段
    ```yaml
    filebeat.inputs:
    - type: log
      enabled: true
      paths:
        - /var/log/tornado/*.log
      exclude_files: ['\.gz$']
    
    output.elasticsearch:
        hosts: ["localhost:9200"]

    processors:
    - decode_json_fields:
        fields: ["timestamp", "level", "name", "message", "extra"]
        process_array: false
        max_depth: 2
        target: ""
        overwrite_keys: false
        add_error_key: true
    ```
    > 可以将`example`下 `tornado` 测试样例产生的日志作为测试
    ```shell
    sudo systemctl status elasticsearch.server  # 确保 ES 有开启
    sudo systemctl start filebeat.server
    sudo systemctl status filebeat.server
    ```
    > 如果安装了**kibana**， 确保**kibana**运行，然后设置 **filebeat** 的 **index**
    ```
    sudo systemctl status kibana.server
    sudo filebeat setup --dashboards
    ```
    > 打开 **kibana** 可以看到解析后的**index**
    ![title](./filebeat_index.png)
    
  - **Logstash** 
    > `filebeat` 也可以将日志发送至 `Logstash` 再由 `Logstash` 进行进一步处理。修改配置文件
    ```yaml
    output.logstash:
      hosts: ["localhost:5044"]
    ```

# 4. **ElasticSearch 日志格式**

```json
{
    "_index": "name_prod|dev|test_2020_10_30",
    "_type": "log",
    "_source": {
        "@timestamp": "2020-10-30T10:18:14.023065",
        "mechineId": "mechinename@192.168.50.1",
        "name": "AutoTableBackend",
        "level": "ERROR",
        "server": "default_server",
        "app": "default_app",
        "message": "this is a info message",
        "extra": {
            "location": "/opt/dms/utils/esclient.py:20",
            "requestid": "535419c2-f1e9-4144-9973-18ef64372e1d",
        },
    }
}
```
### **格式说明**
- _index
    - ElasticSearch 索引名称
    - 日期+日志文件名+运行环境
    - 运行环境test|prod|dev
    - prod可不加
    - 注意不要包含一些特殊字符，如 `{` `-`
- _type
    - 索引类型
        - log 日志类型
- _source: 具体内容
    - @timestamp
        - date
        - 时间 iso格式
    - name
        - keyword
        - 日志名
    - level
        - keyword
        - 日志级别
    - server
        - keyword
        - 服务名称
    - app
        - keyword
        - 应用名称
    - message
        - text
        - 日志内容
        - ERROR级别错误信息自动加入到此
    - extra
        其他上述无涉及字段，例如: 
        - requestid: web应用中追踪访问请求链路
        - location: 日志行定位，在ERROR日志中，可快速定位错误行
