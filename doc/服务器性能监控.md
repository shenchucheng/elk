# **Prometheus** 服务器性能监控

## 1. **介绍**
- [**Prometheus**](https://prometheus.io/)
    > Prometheus 是由 SoundCloud 开源监控告警解决方案，一站式实现监控告警，其依赖少，功能齐全，支持对主机/云/容器的监控，数据查询语句表现力更强大，内置更强大的统计函数。

- [**Grafana**](https://grafana.com/)
    > Grafana是一个开源的数据可视化平台，通过配置接入各种数据源，就可以查询和可视化数据。Grafana支持接入当前各种主流的数据库，并且能将各数据库中的数据以非常灵活酷炫的图表展现出来，同时也因为是开源软件方便二次开发定制。Grafana完全支持prometheus，并有大量插件可供使用。

## 2. **安装**
- [**prometheus**](https://prometheus.io/docs/prometheus/latest/installation)
    - [**二进制文件安装**](https://prometheus.io/download/#prometheus)
        > 下载对应二进制包，并用 `systemctl`  管理，以`prometheus-2.22.0.linux-amd64` 安装到 `/opt` 目录为例：

        ```shell
        # 添加运行用户
        groupadd prometheus
        useradd -g prometheus -d /var/lib/prometheus -s /sbin/nologin prometheus
        # 新建数据存储目录
        sudo mkdir /var/lib/prometheus
        # 下载二进制包并解压
        wget https://github.com/prometheus/prometheus/releases/download/v2.22.0/prometheus-2.22.0.linux-amd64.tar.gz
        tar -xzvf prometheus-2.22.0.linux-amd64.tar.gz
        # 移动至/opt目录
        sudo mv prometheus-2.22.0.linux-amd64 /opt/prometheus
        cd /opt/prometheus
        # prometheus.yml 配置文件 
        # 详细见文件配置
        # 分配权限
        sudo chown -R prometheus:prometheus /opt/prometheus
        # 运行测试
        sudo -u prometheus /opt/prometheus/prometheus --config.file=/opt/prometheus/prometheus.yml --web.console.templates=/opt/prometheus/consoles --web.console.libraries=/opt/prometheus/console_libraries --storage.tsdb.path /tmp/prometheus --web.listen-address=0.0.0.0:9090 
        # 会显示verbose信息，如果启动成功，访问http://localhost:9090，测试成功后Ctrl+C退出程序
        ```
        > 使用 `systemctl` 工具管理 新建 `promethes.service`u 文件，写入内容：
        ```
        [Unit]
        Description=Prometheus
        Documentation=https://prometheus.io/docs/introduction/overview/
        Wants=network-online.target
        After=network-online.target

        [Service]
        Type=simple
        User=prometheus
        Group=prometheus
        ExecReload=/bin/kill -HUP $MAINPID
        ExecStart=/opt/prometheus/prometheus --config.file=/opt/prometheus/prometheus.yml --web.console.templates=/opt/prometheus/consoles --web.console.libraries=/opt/prometheus/console_libraries --storage.tsdb.path=/var/lib/prometheus --web.listen-address=0.0.0.0:9090
    
        SyslogIdentifier=prometheus
        Restart=always

        [Install]
        WantedBy=multi-user.target

        ```
        > 将上述文件保存，并添加service服务
        ```
        sudo chown prometheus:prometheus promethes.service
        sudo ln -s /opt/prometheus/prometheus.service /usr/lib/systemd/system
        sudo systemctl daemon-reload 
        sudo systemctl start promethes.service
        sudo systemctl status promethes.service
        sudo systemctl enable promethes.service
        ```
        

    - [**docker 安装**](https://prometheus.io/docs/prometheus/latest/installation/#using-docker)

- [**grafana**](https://grafana.com/docs/grafana/latest/installation/)
    > grafana 提供了多种方式进行安装，除了提供 `deb/rpm` 包，也支持 `apt/yum` 安装，另外还可以 `docker` 安装。
    - [**debian**](https://grafana.com/docs/grafana/latest/installation/debian/)
    - [**rpm**](https://grafana.com/docs/grafana/latest/installation/rpm/)
    - [**docker**](https://grafana.com/docs/grafana/latest/installation/docker/)
    - [**grafana**](https://grafana.com/grafana/download)
    
    > 以 `yum` 在 `centos7` 安装 `grafana` 为例，新建文件 `/etc/yum.repos.d/grafana.repo`，添加 `grafana` 的 `yum` 源
    ```
    [grafana]
    name=grafana
    baseurl=https://packages.grafana.com/oss/rpm
    repo_gpgcheck=1
    enabled=1
    gpgcheck=1
    gpgkey=https://packages.grafana.com/gpg.key
    sslverify=1
    sslcacert=/etc/pki/tls/certs/ca-bundle.crt
    ```

    > 然后 `yum` 安装
    ```
    sudo yum update
    sudo yum install grafana
    sudo systemctl daemon-reload
    sudo systemctl start grafana-server
    sudo systemctl status grafana-server.service
    ```

    > 访问 `http://localhost:3000` ，初试账号密码都是 `admin` ，登陆完就可以看到 `grafana` 的面板， 此时是没有数据源的，为获取 `prometheus` 数据，需要添加 `prometheus` 的数据源，步骤如下：
    ![grafana_step_1](./images/grafana_step_1.png)
    ![grafana_step_2](./images/grafana_step_2.png)
    ![grafana_step_3](./images/grafana_step_3.png)


## 3. **插件**
### 3.1 [**node_exporter**](https://github.com/prometheus/node_exporter)
- **介绍**
    > 用于采集主机的运行指标如CPU, 内存，磁盘等信息。
- **安装**
    - [**二进制安装**](https://prometheus.io/download/#node_exporter)
        > 同样以 `node_exporter-1.0.1.linux-amd64` 为例，我们将  `exporter` 安装到 `/opt/exporters` 目录下
        ```
        sudo mkdir /opt/exporters
        cd ~
        wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz
        tar -xvzf node_exporter-1.0.1.linux-amd64.tar.gz
        sudo mv node_exporter-1.0.1.linux-amd64 /opt/exporters/node_exporter
        cd /opt/exporters/node_exporter 
        # 进行测试
        ./node_exporter --web.listen-address=0.0.0.0:9100 
        # 成功运行后访问 http://localhost:19100/metrics 可以看到具体信息，Ctrl+C 退出 
        ``` 
        > 使用 `systemctl` 管理，新建 `node_exporter.service` 文件，加入内容：
        ```service
        [Unit]
        Description=node_exporter
        After=network.target
        
        [Service]
        Type=simple
        User=prometheus
        ExecStart=/opt/exporters/node_exporter/node_exporter
        Restart=on-failure

        [Install]
        WantedBy=multi-user.target
        ```
        > 将上述文件保存，并添加service服务
        ```shell
        sudo chown -R prometheus:prometheus /opt/exporters
        sudo ln -s /opt/exporters/node_exporter/node_exporter.service /usr/lib/systemd/system
        sudo systemctl daemon-reload 
        sudo systemctl start promethes.service
        sudo systemctl status promethes.service
        sudo systemctl enable promethes.service  # 开机启动
        ```
    - [**docker 安装**](https://github.com/prometheus/node_exporter#using-docker)
        > 注意 `Node_exporter` 用于监视主机系统，不建议将其部署为  `Docker` 容器，因为它需要访问主机系统。使用 `Docker` 安装注意事项见 `github` 链接。

- **使用**
    > 测试正常后，为了使 `Prometheus` 采集到 `node_exporter` 的信息，需要修改配置文件，修改 `/opt/prometheus/prometheus.yml` 文件
，在 scrape_configs 下新增采集任务。
    ```yaml
    scrape_configs:
      - job_name: 'prometheus'
        static_configs:
          - targets: ['localhost:9090']

      - job_name: 'node'
          static_configs:
          - targets: ['localhost:9100']
    ```
    > 将上面的文件修改完成后保存，重启 `prometheus` 服务。
    ```shell
    sudo systemctl restart prometheus
    ```
    
    > 访问 `http://localhost:9090/graph` 搜索框输入 `up{job="node"}` 然后执行，可以看到返回结果 `value` 为 `1` 说明正常。

    > 事实上，prometheus 主要任务负责数据的收集，存储并且对外提供数据查询支持，并不直接服务监控特定的目标，因此为了能够能够监控特定指标，我们必须使用到各种Exporter，node_exporter 只是其中一种，如果使用上面 static_configs 配置任务的方法，那么每次都需要重启  prometheus 服务。 因此 prometheus 支持服务发现。下面介绍基于文件的服务发现：

    > 修改主配置文件 `/opt/prometheus/prometheus.yml` 
    ```yaml
    scrape_configs:
      - job_name: 'prometheus'
        static_configs:
        - targets: ['localhost:9090']
      - job_name: 'file_sd'
        file_sd_configs:
        - files: ['/opt/prometheus/files_sd/*.json']
    ```
    > 然后在 `/opt/prometheus/files_sd/` 文件夹下，新建 `test.json` 文件，加入以下内容

    ```json
    [  
        {
            "targets": [ "localhost:9100"],
            "labels": {
                "env": "prod",
                "job": "node"
            }
        }
    ]
    ```
    > 再一次打开 `prometheus` 监控面板，查看 `Status` 下的 `Targets` ，配置正确就可以看到 `files_sd` 下，`node` 服务已被自动加入。
    
    > ![](./images/prometheus_status_targets.png)

- **效果**
    > 使用 `prometheus+grafana` 的最方便之处之一就是，有大量插件可直接使用，接下来是使用 `Grafana` 可视化输出，直接使用 [Node Exporter for Prometheus Dashboard CN](https://grafana.com/grafana/dashboards/8919) 插件进行可视化输出，这是一个在 `Grafana` 支持 `node_exporter` 的可视化输出的中文插件。更多可视化插件可以在 https://grafana.com/grafana/dashboards 中查找。下面介绍如何添加可视化面板：

    ![grafana_dashboard_import_1](./images/grafana_dashboard_import_1.png)
    ![grafana_dashboard_import_2](./images/grafana_dashboard_import_2.png)
    ![grafana_dashboard_import_3](./images/grafana_dashboard_import_3.png)
      
    > 可视化效果
    ![grafana_dashboard_node](./images/grafana_dashboard_node.png)

> 以上通过 `node_exporter` 作为例子，较为详细地介绍了如何为 `prometheus` 安装 `exporter` ，并用 `grafana` 做可视化输出。接下来将较为简略地介绍几款 `exporter` 及可视化插件。

### 3.2 [**mysqld_exporter**](https://github.com/prometheus/mysqld_exporter)
> 这是 `prometheus` 官方提供的用于监控 `mysql` 运行状态的 `exporter`，支持 `docker` 安装。二进制安装[**链接**](https://github.com/prometheus/mysqld_exporter/releases)，同样下载并解压，移动到 `/opt/exporters/mysqld_exporter`，下面提供 `mysqld_exporter.service` 文件模板。
```
[Unit]
Description=mysqld_exporter
Document=https://github.com/prometheus/mysqld_exporter
After=network.target

[Service]
Type=simple
User=prometheus
ExecStart=/opt/exporters/mysqld_exporter/mysqld_exporter --config.my-cnf=/opt/exporters/mysqld_exporter/mysqld_exporter.conf
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
> `mysqld_exporter` 信息采集需要登陆到 `mysql` 中，所以要配置用户登陆信息，建议在 `mysql` 中新建用户 `exporter`，`mysql` `root` 用户下：
```
CREATE USER 'exporter'@'localhost' IDENTIFIED BY 'XXXXXXXX' WITH MAX_USER_CONNECTIONS 3;
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'localhost';
```
> 新建配置文件 `/opt/exporters/mysqld_exporter/mysqld_exporter.conf`
```
[client]
port=3306
user=exporter
password=XXXXXXXX
```

> 修改 `prometheus` 的 `/opt/prometheus/files_sd/test.json` 文件
```
[
  {
    "targets": [ "localhost:9100"],
    "labels": {
      "env": "prod",
      "job": "node"
    }
  },
  {
    "targets": [ "localhost:9104"],
    "labels": {
      "env": "test",
      "job": "mysqld"
    }
  }
]
```
> 在 `prometheus` 管理面板的 `Targets` 下，此时 `file_sd` 里面将出现新增的 `mysqld` ，然后在 `grafana` 下新增 `mysqld_exporter` 可视化面板，选择 [**MySQL Overview**](https://grafana.com/grafana/dashboards/7362)，这个面板 `id` **7362**
 
![grafana_dashboard_mysql](./images/grafana_dashboard_mysql.png)

### 3.3 [**redis_exporter**](https://github.com/oliver006/redis_exporter)
- [**二进制安装**](https://github.com/oliver006/redis_exporter/releases)
- [支持 **docker**](https://github.com/oliver006/redis_exporter#run-via-docker)
- [**k8s**](https://github.com/oliver006/redis_exporter#run-via-docker)

- [可视化输出](https://grafana.com/dashboards/763) grafana id 763

- `prometheus` 的服务发现与 `grafana` 面板添加 参考上述例子。

### 3.3 [**mongodb_exporter**](https://github.com/percona/mongodb_exporter)
- [二进制下载](https://github.com/percona/mongodb_exporter/releases)
- [支持 docker](https://github.com/percona/mongodb_exporter/releases#building-and-running)
- [**k8s**](https://github.com/helm/charts/tree/master/stable/prometheus-mongodb-exporter)
- [grafana](https://grafana.com/grafana/dashboards/7353)


### 3.4 **kong**
- 以下默认安装了 **kong**
- kong 本身有 metrics 接口，但是默认不开启，通过以下开启
  ```
  curl -XPOST http://localhost:8001/services -d name=prometheusEndpoint -d url=http://localhost:8001/metrics
  curl -XPOST http://localhost:8001/services/prometheusEndpoint/routes -d paths[]=/metrics
  ```
- **kong** 官方有提供 **grafana** [插件](https://grafana.com/dashboards/7424) `id` **7424**



### 3.5 [**NVIDIA GPU Monitoring Tools**](https://github.com/NVIDIA/gpu-monitoring-tools#dcgm-exporter)
- NVIDIA GPU 监控 (未测试)